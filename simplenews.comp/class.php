<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;
use \Bitrix\Main\Data\Cache;

class classSimplenews extends CBitrixComponent 
{
	protected $navigation;
	protected $arFilter;
	protected $yearList;	
	protected $year;

    private function _checkModules() {
        if (!Loader::includeModule('iblock')) 
        {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }

        return true;
    }

    private function _checkParams() {

    	if (!isset($this->arParams["IBLOCK_ID"])) {
    		throw new \Exception('Не указан ID инфоблока');
    	}

    	if (!isset($this->arParams["LIMIT"])) {
    		throw new \Exception('Не указано количество новостей на странице');
    	}

        return true;
    }

	public function executeComponent() 
	{

		$this->_checkModules();
		$this->_checkParams();

		$this->initNavParams();
		$this->initYearList();
		$this->initFilter();
		$this->initTabLinks();

		if ($this->startResultCache(false, $this->getAdditionalCacheId()))
		{
			$this->getElementList();

			$this->arResult["NAV_PARAMS"] = $this->navParams;

    		$this->IncludeComponentTemplate();
		}

		global $APPLICATION;
		$APPLICATION->SetTitle("Список новостей ".$this->arResult["ROWS_COUNT"]." шт.");

	}


	protected function getFilterYear() 
	{
		$request = Application::getInstance()->getContext()->getRequest(); 

		$year = $request->get("year");

		if (in_array($year, $this->yearList)) {
			$this->year = $year;
		} else {
			$this->year = $this->yearList[0];
		}

	}

	protected function initFilter() 
	{
		$this->getFilterYear();

		$arFilter = [
			"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
			"ACTIVE" => 'Y',
			$this->getDateFilter($this->year)
		];

		$this->arFilter = $arFilter;

	}

	protected function getDateFilter($year) 
	{
		global $DB;
		$arFilter = array(
  			">=DATE_CREATE"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,1,1,$year)),
  			"<=DATE_CREATE"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(23,59,59,12,31,$year)),
  		);

		return $arFilter;
	}

	protected function initYearList() 
	{

		$iblockID = $this->arParams["IBLOCK_ID"];
		$cachePath = 'simplenews';
		$cacheID = $iblockID.$cachePath;

 		$obCache = new CPHPCache;
		if($obCache->InitCache(36000, $cacheID, $cachePath))
 		{
     		$this->yearList = $obCache->GetVars()["yearList"];
 		}
 		elseif($obCache->StartDataCache())
 		{	

 			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache('simplenews');

			$connection = \Bitrix\Main\Application::getConnection();
			$tableName = \Bitrix\Iblock\ElementTable::getTableName();

			$sql = 'SELECT DATE_CREATE
			FROM '.$tableName.'
			WHERE `IBLOCK_ID` = '.$iblockID.'
			GROUP BY DATE_FORMAT(DATE_CREATE, "%y")
			ORDER BY DATE_CREATE DESC';
    		
			$recordset = $connection->query($sql);
			while ($row = $recordset->fetch()) {
				$this->yearList[] = $row["DATE_CREATE"]->format('Y');
			}	

			$CACHE_MANAGER->RegisterTag("iblock_id_".$this->arParams["IBLOCK_ID"]);
			$CACHE_MANAGER->EndTagCache();

    		$obCache->endDataCache(["yearList" => $this->yearList]);

    	}

	}

	protected function initTabLinks()
	{	

		$request = Application::getInstance()->getContext()->getRequest();
		$uriString = $request->getRequestUri();
		$uri = new Uri($uriString);

		$uri->deleteParams(["PAGEN_".($GLOBALS['NavNum']+1)]);

		foreach ($this->yearList as $year) {
			$uri->deleteParams(["year"]);	
			$this->arResult["TABS"][$year] = [
				"VALUE" => $year,
				"LINK" => $uri->addParams(["year" => $year])->getUri(),
				"ACTIVE" => ($year == $this->year) ? "Y" : ''
			];
		}

	}

	protected function getElementList() 
	{
		$res = CIBlockElement::GetList(
			['created' => 'desc'], 
			$this->arFilter, 
			false, 
			$this->navParams, 
			['NAME', 'DATE_CREATE', 'PREVIEW_TEXT', 'PREVIEW_PICTURE']
		);

		$this->arResult["ROWS_COUNT"] = $res->SelectedRowsCount();

		$this->arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponenOgject, "", '', false);

		while ($row = $res->Fetch()) 
		{
			$row["PREVIEW_PICTURE"] = CFile::GetFileArray($row["PREVIEW_PICTURE"]);
			$this->arResult['ITEMS'][] = $row;
		}
	}

	protected function getAdditionalCacheId()
  	{	
    	return array(
      		$this->arFilter,
      		$this->navigation,
    	);
  	}


  	protected function initNavParams()
  	{
        $this->navParams = array(
          'nPageSize' => $this->arParams['LIMIT'],
        );

        $this->navigation = \CDBResult::GetNavParams($this->navParams);

    }

}

 ?>