<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"CACHE_SETTINGS" => array(
			"NAME" => "Настройки кеширования",
			"SORT" => 600
		),
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"NAME" => 'ID инфоблока новостей',
			"TYPE" => 'STRING',
			"PARENT" => 'BASE'
		),
		"LIMIT" => Array(
			"NAME" => 'Число новостей на 1-ой странице',
			"TYPE" => 'NUMBER',
			"PARENT" => 'BASE'
		),
		"CACHE_TYPE" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "Тип кеширования",
			"TYPE" => "LIST",
			"VALUES" => array(
				"A" => "Авто + Управляемое",
				"Y" => "Кешировать",
				"N" => "Не кешировать",
			),
			"DEFAULT" => "N",
			"ADDITIONAL_VALUES" => "N",
		),
		"CACHE_TIME" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "Время кеширования, (сек.)",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => 3600,
			"COLS" => 5,
		),
	)
);
?>
