<?php 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
 <section>
 	<div class="simplenewslist">
 		<div class="container ">
 			<div class="row">
 				<div class="col-12">
 					<h1><?$APPLICATION->ShowTitle(false);?></h1>
 				</div>
 			</div>
 			<div class="row">
 				<div class="col-12 tabs">
 					<?php foreach ($arResult["TABS"] as $tab): ?>
 						<?php if ($tab["ACTIVE"] == "Y"): ?>
 							<span><?=$tab["VALUE"]?></span>
 						<?php else: ?>
 							<a href="<?=$tab["LINK"]?>"><?=$tab["VALUE"]?></a>
 						<?php endif; ?>
 					<?php endforeach ?>
 				</div>
				<div class="col-12">
					<div class="row">
						<?php foreach ($arResult["ITEMS"] as $item): ?>
							<div class="col-12 newsItem">
								<div class="row">
									<div class="col-12">
										<h4><?=$item["NAME"]?></h4>
									</div>
									<div class="col-12 col-md-3">
											<img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$item["NAME"]?>" title="<?=$item["NAME"]?>">
									</div>
									<div class="col-12 col-md-9">
										<?=$item["PREVIEW_TEXT"]?>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
 				
 			</div>
 			<div class="row">
 				<div class="col-12">
 					<? echo $arResult["NAV_STRING"]; ?>
 				</div>
 			</div>
 		</div>
 	</div>
 	
 </section>